<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/login-oauth.html.twig */
class __TwigTemplate_6a7076f37dc0ed2dbc48199b6a7736023c60671a21a23ca49c849296772e84d0 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ($this->getAttribute(($context["oauth"] ?? null), "enabled", [])) {
            // line 2
            echo "    <div class=\"form-oauth button-group\">
        ";
            // line 4
            echo "        <input type=\"submit\" name=\"task\" value=\"login.login\" tabindex=\"-1\" />

        ";
            // line 7
            echo "        <p>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->translate($this->env, "PLUGIN_LOGIN_OAUTH.OAUTH_CONNECT_MESSAGE"), "html", null, true);
            echo "

        ";
            // line 9
            if (twig_test_empty($this->getAttribute(($context["oauth"] ?? null), "providers", []))) {
                // line 10
                echo "            <p>";
                echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->translate($this->env, "PLUGIN_LOGIN_OAUTH.ENABLE_A_PROVIDER"), "html", null, true);
                echo "</p>
        ";
            }
            // line 12
            echo "
        ";
            // line 13
            if ((count($this->getAttribute(($context["oauth"] ?? null), "providers", [])) > 4)) {
                // line 14
                echo "            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->translate($this->env, "PLUGIN_LOGIN_OAUTH.OR"), "html", null, true);
                echo " <label for=\"oauth-input\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->translate($this->env, "PLUGIN_LOGIN_OAUTH.OAUTH_CONNECT_MESSAGE_EXTRA"), "html", null, true);
                echo "</label></p>
            <input type=\"checkbox\" id=\"oauth-input\" />
            <ul class=\"oauth-provider-extra\">
                ";
                // line 17
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute(($context["oauth"] ?? null), "providers", []), 4, null));
                foreach ($context['_seq'] as $context["provider"] => $context["credentials"]) {
                    // line 18
                    echo "                    ";
                    $context["icon"] = (($this->getAttribute(($context["icons"] ?? null), $context["provider"], [], "array", true, true)) ? (_twig_default_filter($this->getAttribute(($context["icons"] ?? null), $context["provider"], [], "array"), twig_lower_filter($this->env, $context["provider"]))) : (twig_lower_filter($this->env, $context["provider"])));
                    // line 19
                    echo "                    <li><button name=\"oauth\" value=\"";
                    echo twig_escape_filter($this->env, $context["provider"], "html", null, true);
                    echo "\" type=\"submit\" class=\"button ";
                    echo twig_escape_filter($this->env, twig_lower_filter($this->env, $context["provider"]), "html", null, true);
                    echo "\" href=\"";
                    echo twig_escape_filter($this->env, ($context["base_url_relative"] ?? null), "html", null, true);
                    echo "/login/oauth";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["config"] ?? null), "system", []), "param_sep", []), "html", null, true);
                    echo twig_escape_filter($this->env, $context["provider"], "html", null, true);
                    echo "\" title=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->translate($this->env, "PLUGIN_LOGIN_OAUTH.OAUTH_LOGIN", ucwords($context["provider"])), "html", null, true);
                    echo "\"><i class=\"fa fa-";
                    echo twig_escape_filter($this->env, ($context["icon"] ?? null), "html", null, true);
                    echo "\"></i> ";
                    echo twig_escape_filter($this->env, ucwords($context["provider"]), "html", null, true);
                    echo "</button></li>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['provider'], $context['credentials'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 21
                echo "            </ul>
        ";
            } else {
                // line 23
                echo "            </p>
        ";
            }
            // line 25
            echo "
        <ul class=\"oauth-provider\">
            ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute(($context["oauth"] ?? null), "providers", []), 0, 4));
            foreach ($context['_seq'] as $context["provider"] => $context["credentials"]) {
                // line 28
                echo "                ";
                $context["icon"] = (($this->getAttribute(($context["icons"] ?? null), $context["provider"], [], "array", true, true)) ? (_twig_default_filter($this->getAttribute(($context["icons"] ?? null), $context["provider"], [], "array"), twig_lower_filter($this->env, $context["provider"]))) : (twig_lower_filter($this->env, $context["provider"])));
                // line 29
                echo "                <li><button name=\"oauth\" value=\"";
                echo twig_escape_filter($this->env, $context["provider"], "html", null, true);
                echo "\" type=\"submit\" class=\"button ";
                echo twig_escape_filter($this->env, twig_lower_filter($this->env, $context["provider"]), "html", null, true);
                echo "\" href=\"";
                echo twig_escape_filter($this->env, ($context["base_url_relative"] ?? null), "html", null, true);
                echo "/login/oauth";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["config"] ?? null), "system", []), "param_sep", []), "html", null, true);
                echo twig_escape_filter($this->env, $context["provider"], "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->translate($this->env, "PLUGIN_LOGIN_OAUTH.OAUTH_LOGIN", ucwords($context["provider"])), "html", null, true);
                echo "\"><i class=\"fa fa-";
                echo twig_escape_filter($this->env, ($context["icon"] ?? null), "html", null, true);
                echo "\"></i> ";
                echo twig_escape_filter($this->env, ucwords($context["provider"]), "html", null, true);
                echo "</button></li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['provider'], $context['credentials'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "        </ul>
    </div>
    <span class=\"delimiter\">";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->translate($this->env, "PLUGIN_LOGIN_OAUTH.OR"), "html", null, true);
            echo "</span>
    <p>";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->translate($this->env, "PLUGIN_LOGIN_OAUTH.OAUTH_SIGNIN"), "html", null, true);
            echo "</p>
";
        }
    }

    public function getTemplateName()
    {
        return "partials/login-oauth.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 34,  141 => 33,  137 => 31,  115 => 29,  112 => 28,  108 => 27,  104 => 25,  100 => 23,  96 => 21,  74 => 19,  71 => 18,  67 => 17,  58 => 14,  56 => 13,  53 => 12,  47 => 10,  45 => 9,  39 => 7,  35 => 4,  32 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if oauth.enabled %}
    <div class=\"form-oauth button-group\">
        {# Create hidden duplicate of submit button to designate it as default #}
        <input type=\"submit\" name=\"task\" value=\"login.login\" tabindex=\"-1\" />

        {# Show OAuth authentication providers #}
        <p>{{ 'PLUGIN_LOGIN_OAUTH.OAUTH_CONNECT_MESSAGE'|t }}

        {% if oauth.providers is empty %}
            <p>{{ 'PLUGIN_LOGIN_OAUTH.ENABLE_A_PROVIDER'|t }}</p>
        {% endif %}

        {% if oauth.providers|count > 4 %}
            {{ 'PLUGIN_LOGIN_OAUTH.OR'|t }} <label for=\"oauth-input\">{{ 'PLUGIN_LOGIN_OAUTH.OAUTH_CONNECT_MESSAGE_EXTRA'|t }}</label></p>
            <input type=\"checkbox\" id=\"oauth-input\" />
            <ul class=\"oauth-provider-extra\">
                {% for provider,credentials in oauth.providers[4:] %}
                    {% set icon = icons[provider]|default(provider|lower) %}
                    <li><button name=\"oauth\" value=\"{{ provider }}\" type=\"submit\" class=\"button {{ provider|lower }}\" href=\"{{ base_url_relative }}/login/oauth{{ config.system.param_sep }}{{ provider }}\" title=\"{{ 'PLUGIN_LOGIN_OAUTH.OAUTH_LOGIN'|t(provider|ucwords) }}\"><i class=\"fa fa-{{ icon }}\"></i> {{ provider|ucwords }}</button></li>
                {% endfor %}
            </ul>
        {% else %}
            </p>
        {% endif %}

        <ul class=\"oauth-provider\">
            {% for provider,credentials in oauth.providers[:4] %}
                {% set icon = icons[provider]|default(provider|lower) %}
                <li><button name=\"oauth\" value=\"{{ provider }}\" type=\"submit\" class=\"button {{ provider|lower }}\" href=\"{{ base_url_relative }}/login/oauth{{ config.system.param_sep }}{{ provider }}\" title=\"{{ 'PLUGIN_LOGIN_OAUTH.OAUTH_LOGIN'|t(provider|ucwords) }}\"><i class=\"fa fa-{{ icon }}\"></i> {{ provider|ucwords }}</button></li>
            {% endfor %}
        </ul>
    </div>
    <span class=\"delimiter\">{{ 'PLUGIN_LOGIN_OAUTH.OR'|t }}</span>
    <p>{{ 'PLUGIN_LOGIN_OAUTH.OAUTH_SIGNIN'|t }}</p>
{% endif %}
", "partials/login-oauth.html.twig", "C:\\inetpub\\wwwroot\\grav\\user\\plugins\\login-oauth\\login\\templates\\partials\\login-oauth.html.twig");
    }
}
