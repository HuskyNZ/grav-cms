<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/plugins/darkadmin/blueprints.yaml',
    'modified' => 1541641238,
    'data' => [
        'name' => 'Dark Admin',
        'version' => '1.0.0',
        'description' => 'Modified customadmin plugin to have a nice dark theme matching MacOS Mojave. Special thanks to Romain Fallet, who created the customadmin plugin.',
        'icon' => 'empire',
        'author' => [
            'name' => 'Norman Wink',
            'email' => 'nw@vonheldenundgestalten.de',
            'url' => 'vonheldenundgestalten.de'
        ],
        'homepage' => 'https://github.com/normanwink/grav-darkadmin',
        'keywords' => 'admin, plugin, manager, panel, custom, dark, mojave',
        'bugs' => 'https://github.com/normanwink/grav-darkadmin/issues',
        'readme' => 'https://github.com/normanwink/grav-darkadmin/README.md',
        'license' => 'MIT',
        'dependencies' => [
            0 => 'admin'
        ]
    ]
];
