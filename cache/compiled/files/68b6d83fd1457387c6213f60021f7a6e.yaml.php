<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/accounts/huskynzadmin.yaml',
    'modified' => 1636877429,
    'data' => [
        'state' => 'enabled',
        'email' => 'peter@husky.nz',
        'fullname' => 'Peter',
        'title' => 'Admin',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$RjrlllSLVa6U3pRrE/vXOut5qBlMSqiqIkLUj9mIFSr0VhTL0TcZ.'
    ]
];
