<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/accounts/tu1.yaml',
    'modified' => 1636926434,
    'data' => [
        'state' => 'enabled',
        'email' => 'mail@husky.nz',
        'fullname' => 'Test_User_1',
        'level' => 'Newbie',
        'access' => [
            'site' => [
                'login' => 'true'
            ]
        ],
        'hashed_password' => '$2y$10$.iK4UN3zvY.dj5RbJ3dnfuK3VpOUk7oGkg9PpE1uzrEWQtW0OUb/O',
        'activation_token' => '196d286305dddf891d3488c933d0afb9::1637531234'
    ]
];
