<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://login-oauth2-extras/login-oauth2-extras.yaml',
    'modified' => 1606913908,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'providers' => [
            'gitlab' => [
                'enabled' => false,
                'client_id' => '',
                'client_secret' => '',
                'domain' => NULL,
                'options' => [
                    'scope' => [
                        0 => 'read_user',
                        1 => 'openid'
                    ]
                ]
            ],
            'discord' => [
                'enabled' => false,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'identify',
                        1 => 'email'
                    ]
                ]
            ],
            'slack' => [
                'enabled' => false,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'users:read',
                        1 => 'users:read.email'
                    ]
                ]
            ],
            'jira' => [
                'enabled' => false,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'read:jira-user'
                    ]
                ]
            ],
            'twitch' => [
                'enabled' => false,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'user_read'
                    ]
                ]
            ],
            'azure' => [
                'enabled' => false,
                'tenant' => 'common',
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'openid',
                        1 => 'email',
                        2 => 'profile',
                        3 => 'offline_access',
                        4 => 'User.Read'
                    ],
                    'get_groups' => false,
                    'avatar_max_size' => 240
                ]
            ],
            'patreon' => [
                'enabled' => false,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'users'
                    ]
                ]
            ]
        ],
        'admin' => [
            'enabled' => true,
            'built_in_css' => true,
            'providers' => [
                'gitlab' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'domain' => NULL,
                    'options' => [
                        'scope' => [
                            0 => 'read_user',
                            1 => 'openid'
                        ]
                    ]
                ],
                'discord' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'identify',
                            1 => 'email'
                        ]
                    ]
                ],
                'slack' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'users:read',
                            1 => 'users:read.email'
                        ]
                    ]
                ],
                'jira' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'read:jira-user'
                        ]
                    ]
                ],
                'twitch' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'user_read'
                        ]
                    ]
                ],
                'azure' => [
                    'enabled' => false,
                    'tenant' => 'common',
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'openid',
                            1 => 'email',
                            2 => 'profile',
                            3 => 'offline_access',
                            4 => 'User.Read'
                        ],
                        'get_groups' => false,
                        'avatar_max_size' => 240
                    ]
                ],
                'patreon' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'users'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
